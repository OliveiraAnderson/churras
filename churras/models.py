# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	user_rating = models.IntegerField(default=1)



	@receiver(post_save, sender=User)
	def create_user_profile(sender, instance, created, **kwargs):
	    if created:
	        Profile.objects.create(user=instance)

	@receiver(post_save, sender=User)
	def save_user_profile(sender, instance, **kwargs):
	    instance.profile.save()

class Event(models.Model):
	event_name = models.CharField(max_length=256)
	event_location = models.CharField(max_length=50)
	event_rating = models.IntegerField(default=1)
	event_date =  models.DateField(null=True, blank=True)
	event_creator =  models.ForeignKey(Profile, null=True, blank=True) 
	#event_attendees = models.ManyToManyField(Profile, null=True, blank=True)
	event_created = models.DateTimeField(auto_now_add=True, editable=False)
	event_last_updated = models.DateTimeField(auto_now=True, editable=False)


	class Meta:
		ordering = ('-event_created',)