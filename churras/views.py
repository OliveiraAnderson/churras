# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from churras.models import Event
from churras.models import Profile
from django.contrib.auth.models import User

from rest_framework import viewsets
from churras.serializers import EventSerializer
from churras.serializers import UserSerializer



from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser



@csrf_exempt
def unlock(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        #data = JSONParser().parse(request)
        #print (data)
        return JsonResponse({'fulfillmentText': 'This is a text response'})
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        print (data)
        #if serializer.is_valid():
        #    serializer.save()
        #    return JsonResponse(serializer.data, status=201)
        return JsonResponse({'fulfillmentText': 'This is a text response'})


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Users to be viewed or edited.
    """
    queryset = User.objects.all()  #.order_by('-date_joined')
    serializer_class = UserSerializer


class EventViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Events to be viewed or edited.
    """
    queryset = Event.objects.all()  #.order_by('-date_joined')
    serializer_class = EventSerializer