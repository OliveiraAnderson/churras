from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from welcome.views import index, health

from django.conf.urls import url, include
from rest_framework import routers
from churras import views



router = routers.DefaultRouter()
router.register(r'events', views.EventViewSet)
router.register( r'users', views.UserViewSet)

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', index),
    url(r'^health$', health),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^unlock/', views.unlock)
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

